import pandas as pd
import numpy as np
from pyspark.sql import SparkSession
from pyspark.ml.linalg import Vectors
from pyspark.mllib.linalg import DenseVector
from pyspark.ml.linalg import SparseVector
from pyspark.sql.types import *
from pyspark.sql.functions import udf
from pyspark.ml.linalg import SparseVector, DenseVector
import pyspark.sql.functions as f
from pyspark.ml.linalg import VectorUDT
from pyspark.sql.functions import col
# import xgboost as xgb
import tensorflow as tf
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD
from keras.optimizers import Adam
from keras.utils import to_categorical
from pyspark.sql.functions import monotonically_increasing_id 
from keras.layers.convolutional import Conv1D, Convolution1D
from keras.layers.convolutional import MaxPooling1D
import os
from sklearn.model_selection import train_test_split
import keras
import glob
from pyspark.sql import SparkSession
from pyspark.ml.linalg import SparseVector, DenseVector, Vectors, VectorUDT
from pyspark.sql.types import *
from sklearn.model_selection import train_test_split
from model import *
from keras import backend as K
import pandas as pd
import numpy as np
import os
import pyspark.sql.functions as f
import tensorflow as tf
import numpy as np
from keras.layers import Input, Flatten,Conv1D
from keras.models import Model
from tensorflow.python.keras.utils.data_utils import Sequence




K.tensorflow_backend._get_available_gpus()

os.environ['PYSPARK_PYTHON'] = '/usr/bin/python3.5'
os.environ['PYSPARK_DRIVER_PYTHON'] = '/usr/bin/python3.5'

sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
from keras import backend as K
K.set_image_dim_ordering('tf')
keras.callbacks.ModelCheckpoint("/home/jupyter/learn4fun/ai_platform/keras_model/model", monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)


def model_keras(lenght_feature, batch_size):
    model = Sequential()

    model.add(Conv1D(int(lenght_feature/2048), 64, input_shape=(lenght_feature-1,1), activation='relu') )
    
    model.add(Conv1D(filters=1024, kernel_size=2, padding='valid', activation='relu'))  
    
    model.add(Conv1D(filters=1024, kernel_size=2, padding='valid', activation='relu'))  
    
    model.add(MaxPooling1D(pool_size=9,strides=2)) 
    
    model.add(Conv1D(filters=512, kernel_size=2, padding='valid', activation='relu'))  
    
    model.add(Conv1D(filters=512, kernel_size=2, padding='valid', activation='relu'))  
    
    model.add(MaxPooling1D(pool_size=9,strides=2)) 
    
    model.add(Conv1D(filters=256, kernel_size=6, padding='valid', activation='relu'))  
    
    model.add(Conv1D(filters=256, kernel_size=6, padding='valid', activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=2)) 
    
    model.add(Conv1D(filters=128, kernel_size=12, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=128, kernel_size=12, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=2))  
    
    model.add(Conv1D(filters=64, kernel_size=9, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=64, kernel_size=9, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=2 ))  
    
    model.add(Conv1D(filters=32, kernel_size=6, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=32, kernel_size=6, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=3 ))  
    
    model.add(Conv1D(filters=16, kernel_size=4, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=16, kernel_size=4, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=3 ))  
    
    model.add(Conv1D(filters=8, kernel_size=2, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=8, kernel_size=2, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=3 ))  
    
    model.add(Conv1D(filters=4, kernel_size=9, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=4, kernel_size=9, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=3 ))
    
    model.add(Conv1D(filters=2, kernel_size=6, padding='valid',activation='relu'))  
    
    model.add(Conv1D(filters=2, kernel_size=6, padding='valid',activation='relu'))  

    model.add(MaxPooling1D(pool_size=9,strides=3 ))

    model.add(Flatten())  
    
    model.add(Dense(256))  

    model.add((Activation('relu')))  

    model.add(Dense(128))  

    model.add((Activation('relu')))  

    model.add(Dense(64))  
    
    model.add((Activation('relu')))  
    
    model.add(Dense(2, activation ="softmax"))  

    return model
# class Data_Generator(keras.utils.Sequence) :
#   def __init__(self, data, label, batch_size) :
#     self.data = data
#     self.label = label
#     self.size = len(label)
#     self.batch_size = batch_size
#   def __len__(self) :
#     return (np.ceil(self.size / float(self.batch_size))).astype(np.int)

#   def __getitem__(self, idx) :
#     batch_x = np.expand_dims(self.data[idx * self.batch_size: (idx+1) * self.batch_size].toarray(), axis = 0).astype(int)
#     batch_x= batch_x.reshape((batch_x.shape[1],batch_x.shape[2],1))
#     batch_y = to_categorical(np.squeeze(self.label[idx * self.batch_size: (idx+1) * self.batch_size]),num_classes=2).astype(int)
#     return batch_x, batch_y

# class Data_Generator(keras.utils.Sequence) :
#   def __init__(self, path_data,first_index,last_index, batch_size) :
#     self.path_data = path_data
#     self.batch_size = batch_size
#     self.last_index = last_index
#     self.first_index = first_index
    
#   def __len__(self) :
#     return self.last_index - 1 - self.first_index

#   def __getitem__(self, idx) :
#     idx = self.first_index + idx
#     data,label = get_data(self.path_data+"/"+str(idx)+"/*.libsvm")
    
#     batch_x = np.expand_dims(data.toarray(), axis = 0).astype(int)
#     batch_x = batch_x.reshape((batch_x.shape[1],batch_x.shape[2],1)).astype(int)
    
# #     batch_x = tf.convert_to_tensor(batch_x, np.int32)  
#     batch_y = to_categorical(np.squeeze(label),num_classes=2).astype(int)

# #     batch_y = tf.convert_to_tensor(batch_y, np.int32)
#     print("index", idx)
#     return batch_x, batch_y

class Data_Generator(Sequence) :
    def __init__(self, data, label, first_index,last_index, batch_size) :
        self.data = data
        self.label = label
        self.first_index = first_index
        self.last_index = last_index
        self.batch_size = batch_size
        

    def __len__(self) :
        return self.last_index - 1 - self.first_index

    def __getitem__(self, idx) :
        idx = self.first_index + idx
        data_batch = self.data[idx * self.batch_size: (idx+1) * self.batch_size].toarray().astype(int)
        label_batch = to_categorical(np.squeeze(self.label[idx * self.batch_size: (idx+1) * self.batch_size]),num_classes=2).astype(int)
        return data_batch, label_batch
    
    def dense_to_sparse(self, dense):
        zero = tf.constant(0, dtype=tf.int32)
        where = tf.not_equal(dense, zero)
        indices = tf.where(where)
        values = tf.gather_nd(dense, indices)
        return tf.SparseTensorValue(indices, values, dense.shape)



from joblib import Memory
from sklearn.datasets import load_svmlight_file
# mem = Memory("./mycache")

# @mem.cache
def get_data(path):
    data = load_svmlight_file(glob.glob(path)[0],n_features=4194304)
    return data[0],data[1]


# @mem.cache
# def get_data(path, a):
#     data = load_svmlight_file(path)
#     return data[0],data[1]

def load_data(path, spark):
    orc_df = spark.read.orc(path)
    return orc_df
  
def write_data(path, spark):
    orc_df = spark.read.orc(path)
    return orc_df

def add_empty_col_(v):
    return SparseVector(v.size + 1, v.indices, v.values)